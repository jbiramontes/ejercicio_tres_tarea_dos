package cr.ac.ucenfotec.ejerciciotrestareados.iu;

import java.io.PrintStream;
import java.util.Scanner;

public class IU {
    Scanner input = new Scanner(System.in).useDelimiter("\n");
    PrintStream output = new PrintStream(System.out);


    /**
     * @return opcion numero seleccionado por usuario
     */
    public int leerNumero() {
        return input.nextInt();

    }

    public double leerPrecio() {
        return input.nextDouble();

    }
    /**
     * Mensajes para mostrar al usuario
     *
     * @param mensaje mensaje para mostrar a usuario
     */
    public void mensaje(String mensaje) {
        output.println(mensaje);
    }

    /**
     * Lectura de datos ingresados por usuario
     *
     * @return texto ingresado por el usuario
     */
    public String leerDatos() {
        return input.next();
    }

    public void mostrarMenu(){
        output.println("Bienvenido a su sistema de facturacion");
        output.println("--Seleccione una de las siguientes opciones--");
        output.println("1. Registrar factura");
        output.println("2. Mostrar facturas registradas");
        output.println("3. Salir");

    }






}




