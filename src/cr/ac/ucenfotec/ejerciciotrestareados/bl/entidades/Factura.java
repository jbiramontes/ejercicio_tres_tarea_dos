package cr.ac.ucenfotec.ejerciciotrestareados.bl.entidades;
import cr.ac.ucenfotec.ejerciciotrestareados.Cliente;

import java.time.LocalDate;
import java.util.ArrayList;

public class Factura {
    private String nombreNegocio;
    private String numeroFactura;
    private LocalDate fechaFactura;
    private double total;
    private double impuesto;
    private double subtotal;
    private Cliente clienteFactura;
    private ArrayList<Producto>productosPorFactura;


    public String getNombreNegocio() {
        return nombreNegocio;
    }

    public void setNombreNegocio(String nombreNegocio) {
        this.nombreNegocio = nombreNegocio;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public LocalDate getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(LocalDate fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(double impuesto) {
        this.impuesto = impuesto;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public Cliente getClienteFactura() {
        return clienteFactura;
    }

    public void setClienteFactura(Cliente clienteFactura) {
        this.clienteFactura = clienteFactura;
    }

    public ArrayList<Producto> getProductosPorFactura() {
        return productosPorFactura;
    }

    public void setProductosPorFactura(ArrayList<Producto> productosPorFactura) {
        this.productosPorFactura = productosPorFactura;
    }

    public Factura() {
    }

    public Factura(String nombreNegocio, String numeroFactura, LocalDate fechaFactura, double total, double impuesto, double subtotal, Cliente clienteFactura, ArrayList<Producto> productosPorFactura) {
        this.nombreNegocio = nombreNegocio;
        this.numeroFactura = numeroFactura;
        this.fechaFactura = fechaFactura;
        this.total = total;
        this.impuesto = impuesto;
        this.subtotal = subtotal;
        this.clienteFactura = clienteFactura;
        this.productosPorFactura = productosPorFactura;
    }

    @Override
    public String toString() {
        return "Factura{" +
                "nombreNegocio='" + nombreNegocio + '\'' +
                ", numeroFactura='" + numeroFactura + '\'' +
                ", fechaFactura=" + fechaFactura +
                ", total=" + total +
                ", impuesto=" + impuesto +
                ", subtotal=" + subtotal +
                ", clienteFactura=" + clienteFactura +
                ", productosPorFactura=" + productosPorFactura +
                '}';
    }
}

