package cr.ac.ucenfotec.ejerciciotrestareados;
import java.time.LocalDate;


public class Cliente {

    private String nombre;
    private String identificacion;
    private String genero;
    private LocalDate fecNacimiento;
    private int edad;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public LocalDate getFecNacimiento() {
        return fecNacimiento;
    }

    public void setFecNacimiento(LocalDate fecNacimiento) {
        this.fecNacimiento = fecNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Cliente() {
    }

    public Cliente(String nombre, String identificacion, String genero, LocalDate fecNacimiento, int edad) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.genero = genero;
        this.fecNacimiento = fecNacimiento;
        this.edad = edad;
    }
}
