package cr.ac.ucenfotec.ejerciciotrestareados;

import cr.ac.ucenfotec.ejerciciotrestareados.bl.entidades.LocalDate;
import cr.ac.ucenfotec.ejerciciotrestareados.controlador.Controlador;
import cr.ac.ucenfotec.ejerciciotrestareados.iu.IU;

/**
 * @author Jorge Biramontes
 * @version 1.0
 */
public class Main {


    public static void main(String[] args) {
        Controlador ejecutar = new Controlador();
        ejecutar.ejecutar();


    }
}
