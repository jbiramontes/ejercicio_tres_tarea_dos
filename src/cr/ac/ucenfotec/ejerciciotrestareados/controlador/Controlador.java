package cr.ac.ucenfotec.ejerciciotrestareados.controlador;

import java.time.LocalDate;

import cr.ac.ucenfotec.ejerciciotrestareados.Cliente;
import cr.ac.ucenfotec.ejerciciotrestareados.bl.logica.Gestor;
import cr.ac.ucenfotec.ejerciciotrestareados.iu.IU;

public class Controlador {
    private IU interfaz = new IU();
    private Gestor gestor = new Gestor();

    public void ejecutar() {

        int opcion;


        do {
            interfaz.mostrarMenu();
            opcion = interfaz.leerNumero();
            procesarOpcion(opcion);

        } while (opcion != 3);


    }

    private void procesarOpcion(int opcion) {
        switch (opcion) {
            case 1:
                //  crearFactura();
                break;
            case 2:
                break;
            case 3:
                break;
            default:
                interfaz.mensaje("*** Opcion no valida ***");

        }


    }


    private void crearFactura() {
        interfaz.mensaje("Nombre del negocio");
        String nombreNegocio = interfaz.leerDatos();
        interfaz.mensaje("Numero de factura");
        String numeroFactura = interfaz.leerDatos();
        interfaz.mensaje("Dia");
        int dia = interfaz.leerNumero();
        interfaz.mensaje("Mes");
        int mes = interfaz.leerNumero();
        interfaz.mensaje("Año");
        int anio = interfaz.leerNumero();
        LocalDate fecha = LocalDate.of(anio, mes, dia);
        interfaz.mensaje("Digite el porcentaje de impuesto de ventas");
        double impuesto = interfaz.leerPrecio();
        interfaz.mensaje("Nombre del cliente completo");
        String nombre = interfaz.leerDatos();
        interfaz.mensaje("Numero de identificacion");
        String identificacion = interfaz.leerDatos();
        interfaz.mensaje("Genero Masculina femenino otro");
        String genero = interfaz.leerDatos();
        interfaz.mensaje("Fecha de nacimiento solo numeros enteros POR FAVOR");
        interfaz.mensaje("Dia");
        int diaNacimiento = interfaz.leerNumero();
        interfaz.mensaje("Mes en numero");
        int mesNacimiento = interfaz.leerNumero();
        interfaz.mensaje("Año");
        int anioNacimiento = interfaz.leerNumero();
        LocalDate fecNacimiento = LocalDate.of(anioNacimiento, mesNacimiento, diaNacimiento);
        int edad = 2020 - anioNacimiento;
        interfaz.mensaje("Ingrese los detalles del producto");
        Cliente nuevoCliente=new Cliente(nombre,identificacion, genero,fecNacimiento, edad)

        int opcion;
        do {
            interfaz.mensaje("Digite el codigo del producto");
            String codigoProducto = interfaz.leerDatos();
            interfaz.mensaje("Descripcion del producto");
            String descripcionProducto = interfaz.leerDatos();
            interfaz.mensaje("Digite la cantidad de unidades");
            double cantidadUnidades = interfaz.leerPrecio();
            interfaz.mensaje("Digite el precio del producto");
            double precioProducto = interfaz.leerPrecio();
            gestor.nuevoProductoFactura(codigoProducto, descripcionProducto, cantidadUnidades, precioProducto);
            interfaz.mensaje("Si no desea agregar otro producto digite 0");
            opcion = interfaz.leerNumero();
        } while (opcion != 0);


    }


}
